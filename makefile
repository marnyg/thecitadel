
restart:
	docker-compose restart

up:
	docker-compose up -d

ps:
	docker-compose ps

status:
	docker-compose logs 

dotnet-build-local:
	dotnet

containerTag ?= dev
dotnet-build-container:
	docker build . -t $(containerTag)

dotnet-run:
	dotnet run

