﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace theCitadel.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HostController : ControllerBase
    {
       
        private readonly ILogger<HostController> _logger;

        public HostController(ILogger<HostController> logger) {_logger = logger;}

        [HttpGet]
        public ActionResult<MyHost> Get()
        {
            MyHost a =new MyHost
            { 
                Name=Dns.GetHostName()
            };
            return a;
        }
    }

    
}
